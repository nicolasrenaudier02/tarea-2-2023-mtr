using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BarraProgreso : MonoBehaviour
{
    public Slider slider;
    public float maxTime = 10f;

    private float currentTime;

    void Start()
    {
        currentTime = maxTime;
        InvokeRepeating("UpdateProgressBar", 0f, 0.1f);
    }

    void UpdateProgressBar()
    {
        if (currentTime > 0)
        {
            currentTime -= 0.1f;
            slider.value = currentTime / maxTime;
        }
        else
        {
            // El tiempo ha llegado a su fin, se puede llamar a una funci�n para terminar el juego
            EndGame();
        }
    }

    void EndGame()
    {
        // Aqu� se puede realizar cualquier acci�n para terminar el juego, como cargar una nueva escena o mostrar un mensaje
        SceneManager.LoadScene("RunnerScene");
    }
}